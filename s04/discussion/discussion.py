# Python Class Review

class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")


myObj = SampleClass(2023)

print(myObj.year)
myObj.show_year()


# Fundamentals of OOP
# 1. Encapsulation
# 2. Inheritance
# 3. Polymorphism
# 4. Abstraction

# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit

# In encapsulation, the attributes of a class will be hidden from other classes, and can be accessed only through the methods of their current class. Therefore, it is also known as data hiding.

 # To achieve encapsulation −
 # * Declare the attributes of a class.
 # * Provide getter and setter methods to modify and view the attributes values.

 # Why encapsulation?
 # The fields of a class can be made read-only or write-only.
 # A class can have total control over what is stored in its fields.

 # The prefix underscore is used as a warning for developers that means:
 # "Please be careful about this attribute or method, don’t use it outside of the declared Class"


class Person():
	def __init__(self):
		self._name = "John Doe"
		self._age = 0

	# setter method, set_name
	def set_name(self, name):
		self._name = name

	# getter method, get_name
	def get_name(self):
		print(f"Name of Person: {self._name}")

	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of Person: {self._age}")

p1 = Person()
# print(p1.name)
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()

# Mini-Activity
# Create the necessary getter and setter methods for the protected attributed called age.

# Test Cases
p1.set_age(26)
p1.get_age()


# Inheritance
# The transfer of the characteristics of a parent class to child classes that are derived from it.
# An example of this is an employee and a person. An employee is a person with additional attributes and methods
# To create an inherited class, in the ClassName definition, add the parent class

# Syntax: class ChildClassName(ParentClassName)

class Employee(Person):
	def __init__(self, employeeId):
		super().__init__()
		self._employeeId = employeeId

	# getter 
	def get_employeeId(self):
		print(f"The employee ID is {self._employeeId}")

	# setter
	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# method
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.set_name("Jane Doe")
emp1.get_details()

# class Random(Employee):
# 	def __init__(self, random):
# 		super().__init__()
# 		self._random = random

# 	# getter 
# 	def get_random(self):
# 		print(f"{self._random}")

# 	# setter
# 	def set_random(self, random):
# 		self._random = random

# 	# method
# 	def get_random(self):
# 		print(f"{self._random} belongs to {self._name}")

# rnd = Random("Random Grand Child")
# rnd.get_random()

# Mini-Activity:
# 1. Create a new class called Student that inherits Person with the additional attributes and methods
# attributes:
	# Student No, Course, Year Level

# methods:
	# get_detail: print the output: "<Student Name> is currently in year <year_level> taking up a <course>"
	# necessary getter and setters


class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__()
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	# getters
	def get_student_no(self):
		print(f"Student number of student is {self._student_no}")

	def get_course(self):
		print(f"Course of student is {self._course}")

	def get_year_level(self):
		print(f"The year level of student is {self._year_level}")


	# setters
	def set_student_no(self, student_no):
		self._student_no = student_no

	def set_course(self, course):
		self._course = course

	def set_year_level(self, year_level):
		self._year_level = year_level

	# custom method
	def get_details(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course}.")

# Test Cases:
student1 = Student("stdt-001", "Computer Science", 1)
student1.set_name("Brandon Smith")
student1.set_age(18)
student1.get_details()


# Polymorphism

# A child class inherits all the methods from the parent class. However, in some situations, the method inherited from the parent class doesn’t quite fit into the child class. In such cases, you will have to re-implement method in the child class.

# There are different methods to use polymorphism in Python. You can use different function, class methods or objects to define polymorphism. So, let’s move ahead and have a look at each of these methods in detail.

# Functions and objects
# A function can be created that can take any object, allowing for polymorphism.


class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

# Define a test function that will take an object called obj
def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Create objects instance for Admin and Customer
user_admin = Admin()
user_customer = Customer()

# Pass the created instance to the test_function
test_function(user_admin)
test_function(user_customer)
# What happened is that the test_function would call the methods of the object passed to it, hence allowing it to have different outputs based on the object passed



# Polymorphism with Class Methods
# Python uses two different class types in the same way. 
# An example is a for loop that iterates through a collection of objects. 
# The loop calls the methods without being concerned about which class type each object is.


class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth():
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	# access the occupation method of each item
	person.occupation()

# Polymorphism - allows us to write code that can work with objects of various classes without needing to know the specific classes/types


# Polymorphism with Inheritance
# Method Overriding

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours")


class DeveloperCareer(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours")


class PiShapedCareer(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours")

class ShortCourses(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours")


course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()


# Abstraction
# An abstract class can be considered as a blueprint for other classes. It allows to create a set of methods that must be created within any child classes built from the abstract class
# A class which contains one or more abstract methods is called an abstract class
# Abstracts classes are used to provide a common interface for different implementations
#  By default, Python does not provide abstract classes. Python comes with a module that provides the base for defining Abstract Base classes(ABC) and that module name is ABC

# The import tells the program to get the abc module of python to be used.
from abc import ABC, abstractclassmethod

# The class Polygon inherits the abstract class module
class Polygon(ABC):
	# Create an abstract method called printNumberOfSides that needs to be implemented by classes that inherit Polygon
	@abstractclassmethod
	def printNumberOfSides(self):
		# The pass keyword denotes that the method doesn't do anything.
		pass


class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	# Since the Triangle class inherited the Polygon class, it must now implement the abstract method
	def printNumberOfSides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	# Since the Pentagon class inherited the Polygon class, it must now implement the abstract method
	def printNumberOfSides(self):
		print("This polygon has 5 sides")


shape1 = Triangle()
shape2 = Pentagon()
shape1.printNumberOfSides()
shape2.printNumberOfSides()
