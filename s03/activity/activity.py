class Camper():
    # properties
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    # method
    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")
        
    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program")



name = input("Camper Name: ")
batch = input("Camper Batch: ")
course_type = input("Camper Course: ")

zuitt_camper = Camper(name, batch, course_type)

zuitt_camper.info()
zuitt_camper.career_track()