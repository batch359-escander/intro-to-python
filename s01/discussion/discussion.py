# Comments
# Comments in Python are done using the "#" symbol
# Reminder that comments are not read by the program and only by the user
# Comments can be done in line as well

# ctrl/cmd + / one-line comment

"""
although we have no keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""

# Python Syntax
# Hello world in Python
print("Hello World!")

# Indentation
# Where in other programming languages the indentation in code is for code readability, the indentation in Python is very important.
# In Python, indentation is used to indicate a block of code.
# Similar to JS, there is no need to end statements with semicolons

# Variables
# The terminology used for variable names is identifier.

# Naming convention
# All identifiers should begin with a letter(A to Z), dollar sign($) or an underscore.
# After the first character, identifiers can have any combination of characters.
# Unlike JavaScript that uses camel case, Python uses the snake case convention for variables as defined in the PEP (Python Enhancement Proposal) 8 - Style Guide for Python Code
# A keyword cannot be used as an identifier.

age = 18
middle_initial = "C"
name1, name2, name3, name4 = "John", "Edwin", "Kenneth", "Rey"
print(name4)


# Data Types
# Data types convey what kind of information a variable holds. There are different data types and each type has its own use.

# 1. Strings(str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$w0rd"

# 2. Numbers (int, float, complex) - for integers, decimals and complex numbers
num_of_days = 365 # This is an integer
pi_approx = 3.1416 # This is a decimal
complex_num = 1 + 5j # This is a complex number, j represents the imaginary component

# 3. Boolean (bool) - for truth values
# Boolean values in Python start with Uppercase letters
is_learning = True
is_difficult = False


# Using Variables
# Just like in JavaScript, variables are used by simply calling the name of the identifier
print("My name is " + full_name)

# Terminal Outputs
# print() function
print("Hello World Again")
# To use variables, concatenation ("+" symbol) between strings can be used
print("My name is " + full_name)
# However, see what happens when a number is concatenated.
# print("My age is " + age)
# This returns a type error as numbers can't be concatenated to strings.
# To solve this, typecasting can be used
print("My age is " + str(age))

# Typecasting
# There may be times when you want to specify a type on to a variable. This can be done with casting. Here are some function that can be used:
# 1. int() - converts the value into an integer value
# 2. float() - converts thet value into a float value
# 3. str() - converts the value into strings

print(int(3.5))
print(int("9876"))
print(float(10))


# F-strings
# Another way to avoid the type error in printing without the use of typecasting is the use of F-strings
# To use F-strings, add a lowercase "f" before the string and place the desired variable in {} 
print(f"Hi! My names is {full_name} and my age is {age}")


# Operations
# Arithmetic operators - performs mathematical operations
print(1 + 10) # addition
print(15 - 8) # subtraction
print(18 * 9) # multiplication
print(21 / 7) # division
print(21 // 7) # division
print(2 ** 6) # exponent
print(18 % 4) # remainder


# Assignment Operators - used to assign values to variables
num1 = 3
num1 += 4 # num1 = num1 + 4
# include -=, *=, /=, %=
print(num1)

# logical operators
# AND - and
# OR - or
# NOT - not

print(True and False)
print(not False)
print(False or True)

#comparison operators (returns a boolean value)
print(1 != 1) #false
print(1 == 1)
print(5 > 10)
print(5 < 10)
print(1 <= 10)
print(1 >= 10)
