name = "Warlon"
age = 30
occupation = "Full Stack Developer"
movie = "One Piece Live Action"
rating = 95.99

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 1
num2 = 2
num3 = 3

product = num1 * num2
print(f"The product of {num1} and {num2} is {product}")
num1IsLessThanNum3 = num1 < num3
if(num1IsLessThanNum3):
    print(f"{num1} is less than {num3}")
else: 
    print(f"{num1} is NOT less {num3}")

sum = num3 + num2

print(f"The sum of {num3} and {num2} is {sum}")