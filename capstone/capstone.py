from abc import ABC, abstractclassmethod

class Person():

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # setters
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email
    
    def setDepartment(self, department):
        self._department = department

    
    # getters
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department


    # custom methods
    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def addRequest(self):
        return (f"Request has been added")

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")
    

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    # setters
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email
    
    def setDepartment(self, department):
        self._department = department

    def setMembers(self, members):
        self.members = members

    # getters
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department

    def get_members(self):
        return self._members


    # custom methods
    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")

    def addMember(self, member):
        self._members.append(member)


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # setters
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email
    
    def setDepartment(self, department):
        self._department = department


    # getters
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department


    # methods
    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")

    def addUser(self):
        return (f"User has been added")


class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = ""

    # setters
    def set_name(self, name):
        self._name = name

    def set_requester(self, requester):
        self._requester = requester

    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested
    
    def set_status(self, status):
        self._status = status
    

    # getters
    def get_name(self):
        return self._name

    def get_requester(self):
        return self._requester

    def get_dateRequested(self):
        return self._dateRequested

    def get_status(self):
        return self._status

    # methods

    def updateRequest(self):
        return (f"{self._name} has been updated")
        
    def closeRequest(self):
        return (f"{self._name} has been closed")
        
    def cancelRequest(self):
        return (f"{self._name} has been cancelled")



employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

employee2.login()

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
 print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())