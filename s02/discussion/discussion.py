
# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course!")

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))

# print(type(num1))
# print(f"The sum of num1 and num2 is {num1 + num2}")


# test_num =45
# if test_num >= 60:
# print("test")
# else:
#     print('Test failed')


# test_num2 = int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
#     print("The number is positive")
# elif test_num2 == 0:
#     print("The number is zero")
# else: 
#     print("The number is negative")

# test_num3 = int(input("Please enter the a number \n"))

# if (test_num3 % 3 == 0) and (test_num3 % 5 == 0):
#     print("The number is divisible by both 3 and 5")
# elif test_num3 % 5 == 0:
#     print("The number is divisible by 5")
# elif test_num3 % 3 == 0:
#     print("The number is divisible by 3")
# else: 
#     print("The number is NOT divisible by 3 nor 5")


# i = 1
# while i<= 5:
#     print(f"The current count is {i}")
#     if i == 4: 
#         continue
#     print(f"YES")
#     i += 1
    

# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
    # print(indiv_fruit)


# for x in range(2,9,2):
#     print(f"The current value is {x}")

# count = 0
# for x in range(1,21):
#     if(x % 2 == 0):
#         count += 1
# print(f"The number of even number between 1 and 20 is : {count}")


integer_input = int(input("Please enter any positive integer"))

for x in range(1,11):
    print(f"{integer_input} x {x} = {integer_input * x}")