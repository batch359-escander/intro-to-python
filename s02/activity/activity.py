# validInput = false

# while validInput = false:
while True:
    year = input("Please input a year: ")

    if year.isnumeric() and int(year) > 0:
        break
    else:
        print("Invalid input! Input should be a postive integer, please try again.")
   
year = int(year)

if year % 4 == 0:
    print(f"{year} is a leap year")
else: 
    print(f"{year} is not a leap year")

print("\n")
rows = int(input("Enter number of rows:"))
cols = int(input("Enter number of columns: "))

for x in range(1, rows+1):
    strings = ""
    for y in range(1, cols+1):
        strings += "*"
        if(y == cols):
            print(f"{strings} ")